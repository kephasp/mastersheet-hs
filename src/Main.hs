module Main where
import System.Environment
import Data.Char
import Text.CSV
import Text.Parsec.Error

main :: IO ()
main = do
  fileName <- fmap head getArgs
  applyToCSV fileName $ filter_lines noNaN

applyToCSV :: String -> (CSV -> CSV) -> IO ()
applyToCSV fileName transformer = do
  parse <- parseCSVFromFile fileName
  case parse of
    Left error -> putStrLn $ show error
    Right csv -> do
      writeFile fileName $ printCSV $ transformer csv
                 

filter_lines = filter

noNaN = not . any ((==) "nan" . map toLower)
